import React from 'react';

let HeaderScrollbar = React.createClass({
    url: 'http://123.56.194.182:8080/api/summary',
    getInitialState: function () {
        return {
            data: []
        }
    },
    getData: function () {
        var url = this.url;
        if (!url) {
            return;
        }
        var p = new Promise(function (next, err) {
            $.ajax({
                url: url,
                data: {category: 'recommend'},
                type: 'get'
            }).done(function (res) {
                next(res);
            });
        });
        return p;
    },
    componentDidMount: function () {
        this.getData().then(function (res) {
            if (res && res.adv) {
                this.setState({
                    data: res.adv
                });
                console.log('HeaderScrollbar data loaded');
            }
        }.bind(this));
    },
    render: function () {
        let list = [];
        let data = this.state.data;
        if (data && data.length) {
            for (let item of data) {
                list.push(<div key="1" className="img" style={{backgroundImage:'url(' + item.imageUrl + ')'}}>{item.imageTitle}</div>);
            }
        } else {
            list = <div>Loading...</div>
        }
        return (
            <section className="le-adv-panel ext-narrow">
                <div className="img-panel">
                    {list}
                </div>
            </section>
        )
    }
});

export default HeaderScrollbar;