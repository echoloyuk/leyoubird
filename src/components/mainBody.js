import React from 'react';

let MainBody = React.createClass({
    render: function () {
        return (
            <main role="main" className="ui-content" style={{marginBottom:0}}>
                {this.props.children}
            </main>
        );
    }
});

export default MainBody;