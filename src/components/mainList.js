import React from 'react';
import D from '../dispatch.js';

let MainList = React.createClass({
    url: 'http://123.56.194.182:8080/api/summary',
    getInitialState: function () {
        return {
            data: []
        }
    },
    getData: function () {
        let url = this.url;
        let p = new Promise(function (next, err) {
            $.ajax({
                url: url,
                type: 'get',
                data: {category: 'recommend'}
            }).done(function (res) {
                next(res);
            });
        });
        return p;
    },
    componentDidMount: function () {
        this.getData().then(function (res) {
            if (res && res.summarys) {
                this.setState({
                    data: res.summarys
                });
                console.log('MainList data loaded');
            }
        }.bind(this))
    },
    clickHandler: function (e) {
        let target = e.currentTarget;
        let id = target.getAttribute('data-for');
        let cate = this.props.category;
        let to = this.props.linkUrl;
        to += '?category=' + cate;
        alert(to);
    },
    render: function () {
        let list = [];
        let data = this.state.data;
        if (data && data.length) {
            for (let item of data) {
                list.push(
                    <li className="le-items" key={item.id} data-for={item.id} onTouchEnd={this.clickHandler}>
                        <a className="le-panel">
                            <img src={item.imageUrl} alt="" className="le-image" />
                            <div className="le-image-info">
                                <span>{item.imageTitle}</span>
                            </div>
                        </a>
                    </li>
                );
            }
        } else {
            list = <li>Loading...</li>
        }
        return (
            <section className="main-panel">
                <div className="panel-title">
                    <span>热门行程 特色设计 自由出行</span>
                </div>
                <ul className="le-image-info-panel" id="list">
                    {list}
                </ul>
            </section>
        );
    }
});

export default MainList;