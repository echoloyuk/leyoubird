class Dispatch {
    constructor (id) {
        this.version = '0.9.0';
        this.id = id;
        this._events = {};
    }
    on (name, ...args) {
        console.log(name);
        console.log(...args);
    }
}

export default new Dispatch(new Date().getTime());